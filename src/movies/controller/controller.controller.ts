import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put } from '@nestjs/common';
import { Client, ClientKafka, Ctx, KafkaContext, MessagePattern, Payload } from '@nestjs/microservices';
import { ApiBody, ApiCreatedResponse } from '@nestjs/swagger';
import { microserviceConfig } from 'src/microserviceConfig';
import { MoviesDto } from '../dto/movies.dto';
import { MoviesService } from '../service/movies/movies.service';

@Controller('movie')
export class MoviesController {
    constructor(
        private movieService: MoviesService
    ) {

    }
    @Client(microserviceConfig)
    client: ClientKafka;


    onModuleInit() {
        const requestPatterns = [
            'movie-created', 'movie-create-error'
        ];
        requestPatterns.forEach(pattern => {
            this.client.subscribeToResponseOf(pattern);
        });
    }


    @Get()
    async getAllMovies() {
        return {
            statusCode: HttpStatus.OK,
            data: await this.movieService.getAllMovies(),
        };
    }

    @Post()
    @ApiCreatedResponse({ description: 'Movie creation' })
    @ApiBody({ type: MoviesDto })
    async movieCreateProduce(@Body() data: MoviesDto) {
        let value = this.client.emit<string>('movie-created', JSON.stringify(data));
        return value
    }

    @MessagePattern('movie-created')

    async movieCreateConsume(@Ctx() context: KafkaContext) {
        const originalMessage = context.getMessage();
        console.log("originalm", originalMessage)
        try {
            await this.movieService.createMovie(originalMessage.value)
        }
        catch (_e) {
            let e: Error = _e;
            this.client.emit<string>('movie-create-error', JSON.stringify(e.message));
        }
    }

    @Get(':id')
    @ApiCreatedResponse({ description: 'Get by Movie' })
    async getByMovie(@Param('id') id: number) {
        return {
            statusCode: HttpStatus.OK,
            data: await this.movieService.getByMovie(id),
        };
    }

    @Put(':id')
    async updateMovie(@Param('id') id: number, @Body() data: Partial<MoviesDto>) {
        return {
            statusCode: HttpStatus.OK,
            message: 'Movie update successfully',
            data: await this.movieService.updateMovie(id, data),
        };
    }

    @Delete(':id')
    async deleteMovie(@Param('id') id: number) {
        await this.movieService.deleteByMovie(id);
        return {
            statusCode: HttpStatus.OK,
            message: 'Movie deleted successfully',
        };
    }

}
