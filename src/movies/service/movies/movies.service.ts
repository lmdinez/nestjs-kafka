import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MoviesDto } from 'src/movies/dto/movies.dto';
import { MoviesEntity } from 'src/movies/entity/movies.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MoviesService {
    constructor(
        @InjectRepository(MoviesEntity)
        private moviesRepository: Repository<MoviesEntity>,
    ) {

    }

    async getAllMovies() {
        return await this.moviesRepository.find();
    }

    async createMovie(movieData) {
        const movie = this.moviesRepository.create(movieData);
        try {
            await this.moviesRepository.save(movieData);
            return movie;
        } catch (_e) {
            let e: Error = _e;
        }
        return 1;
    }

    async getByMovie(id: number) {
        return await this.moviesRepository.findOne({ where: { id: id } });
    }

    async updateMovie(id: number, movieData: Partial<MoviesDto>) {
        await this.moviesRepository.update({ id }, movieData);
        return await this.moviesRepository.findOne({ id });
    }

    async deleteByMovie(id: number) {
        await this.moviesRepository.delete({ id });
        return { deleted: true };
    }

}
